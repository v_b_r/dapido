_riders = [];
_markers = [];

_socket = io();

function initMap() {
        
    _socket.on('riders', function(data){
        _riders = data;
    });

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 11,
      center: _center
    });

    for(var i=0;i<_riders.length;i++){
        var marker = new google.maps.Marker({
          position: _riders[i]["location"],
          map: map,
          title: _riders[i]["name"]
        });
        _markers.push(marker);
    }        
}