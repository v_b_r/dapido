/*
* This file contains utility functions for server
*/
module.exports = {
	moveRiderByDelta: function(location){
		var lat_luck_factor = Math.round(Math.random());
		var lat_direction = Math.round(Math.random())==0?1:-1;
		var delta = (Math.floor(Math.random()*100)/10000) * lat_luck_factor;
		var lat_tmp = location["lat"] + delta* lat_direction;
		if(lat_tmp>_lat_limits["max"] || lat_tmp<_lat_limits["min"]){
			location["lat"] = location["lat"] + delta* lat_direction* -1; // reversing if rider is crossing boundary
		}

		var lng_luck_factor = Math.round(Math.random());
		var lng_direction = Math.round(Math.random())==0?1:-1;
		var delta = (Math.floor(Math.random()*100)/10000) * lng_luck_factor;
		var lng_tmp = location["lng"] + delta* lng_direction;
		if(lng_tmp>_lng_limits["max"] || lng_tmp<_lng_limits["min"]){
			location["lng"] = location["lng"] + delta* lng_direction* -1; // reversing if rider is crossing boundary
		}
		return location;
	},

	generateRandomRiders: function(n){
		var riders = [];
		var rider_ids = [];
		for(var i=0;i<n;i++){
			var id = "";
			while(true){
				id = getRandomString(4);
				if(rider_ids.indexOf(id)==-1)
					break;
			}
			rider_ids.push(id);

			// Using id as name - for simplicity; setting all riders as available
			riders[id] = {"name":id,"location":this.getRandomPosition()};
		}
		return riders;
	},
	getRandomPosition: function(){
		return {"lat":getValueInBetween(_lat_limits["min"],_lat_limits["max"]),"lng":getValueInBetween(_lng_limits["min"],_lng_limits["max"])};
	},
	getValuesArray: function(dataObject){
		var dataArray = new Array;
		for(var o in dataObject) {
		    dataArray.push(dataObject[o]);
		}
		return dataArray;
	}
};

// Internal Helper Functions
function getValueInBetween(min,max){
	return Math.random() * (max - min) + min;
}

function getRandomString(length){
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}